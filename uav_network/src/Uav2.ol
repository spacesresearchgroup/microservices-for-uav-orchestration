constants {
  // ORCHESTRATOR_LOCATION = "datagram://172.19.0.3:5683",
  // UAV_LOCATION = "datagram://172.19.0.6:5685",
  ORCHESTRATOR_LOCATION = "datagram://localhost:5683",
  UAV_LOCATION = "datagram://localhost:5685",
  ID = 2,
  ENERGY = 97.0, 
  POSITION_X = 50.0 ,
  POSITION_Y = 78.9,
  ALERT_X = -1.0,
  ALERT_Y = -1.0
}

include "UavSimulator.ol"
