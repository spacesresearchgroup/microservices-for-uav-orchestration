constants {
  // ORCHESTRATOR_LOCATION = "datagram://172.19.0.3:5683",
  // UAV_LOCATION = "datagram://172.19.0.6:5684",
  ORCHESTRATOR_LOCATION = "datagram://localhost:5683",
  UAV_LOCATION = "datagram://localhost:5684",
  ID = 1,
  ENERGY = 99.7, 
  POSITION_X = 75 ,
  POSITION_Y = 35.6,
  ALERT_X = -1.0,
  ALERT_Y = -1.0
}

include "UavSimulator.ol"