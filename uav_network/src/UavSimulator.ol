include "file.iol"
include "ini_utils.iol"
include "time.iol"

include "interface.iol"

inputPort UAV {
  Location: UAV_LOCATION
  Protocol: coap
  Interfaces: UAVInterface 
}

outputPort LogicEngine {
  Protocol: coap
  Interfaces: LogicEngineInterface
}

define register
{
  with( RegisterRequest ){
    .id = int( ID ) ;
    .address = global.inputPorts.UAV.location
  } ;
  register@LogicEngine( RegisterRequest )
}

define position
{
  with( PositionRequest ){
    .uav << {
      .id = int( ID )
    } ;
    .position << {
      .x = double( POSITION_X ),
      .y = double( POSITION_Y )
    }
  } ;
  position@LogicEngine( PositionRequest )
}

define energy
{
  with( EnergyRequest  ){
    .uav << {
      .id = int( ID )
    };
    .energy << {
      .percentage = double( ENERGY_LEVEL )
    }
  } ;
  energy@LogicEngine( EnergyRequest )
}

define alert
{
  with( AlertRequest ){
    .uav << {
      .id = int( ID )
    } ;
    .position << {
      .x = double( ALERT_X ),
      .y = double( ALERT_Y )
    } ;
    .result << {
      .accuracy = 93.0,
      .class = "traffic collision"
    } 
  } ;
  alert@LogicEngine( AlertRequest )
}

define image
{
  getServiceDirectory@File()( dir ) ;
  with ( readFileRequest ) {
    .filename = dir + "/../data/2.jpg" ;
    .format = "base64"
  } ;
  readFile@File( readFileRequest )( readFileResponse ) ;
  with( ImageRequest ) {
    .uav << {
      .id = int( ID )
    } ;
    .image << {
      .base64 = "supposeitsiabase64image"
    }
  } ;
  image@LogicEngine( ImageRequest )
}

init
{
  LogicEngine.location = ORCHESTRATOR_LOCATION ;
  
    register ;
    sleep@Time( 2000 )() ;
    position ;
    sleep@Time( 2000 )() ;
    energy ;
    sleep@Time( 2000 )() 
}

execution{ concurrent }

main
{
  [ policy( PolicyRequest ) ] { 
    if ( PolicyRequest.action.name == "image" ) {
      sleep@Time( 2000 )() ;
      image
    } else if ( PolicyRequest.action.name == "detect" && ( double( ALERT_X ) != -1.0 || double( ALERT_Y ) != -1.0 ) ) {
      sleep@Time( 2000 )() ;
      alert
    }
  } 
}
