constants {
  // ORCHESTRATOR_LOCATION = "datagram://172.19.0.3:5683",
  // UAV_LOCATION = "datagram://172.19.0.6:5686",
  ORCHESTRATOR_LOCATION = "datagram://localhost:5683",
  UAV_LOCATION = "datagram://localhost:5686",
  ID = 3,
  ENERGY = 96.5, 
  POSITION_X = 25.0,
  POSITION_Y = 35.6,
  ALERT_X = 25.0,
  ALERT_Y = 35.6
}

include "UavSimulator.ol"
