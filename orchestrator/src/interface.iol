type UAV: void {
  .id: int
  .address?: string
  .currentPosition?: Position
  .currentEnergy?: Energy
}

type Position: void {
  .x: double
  .y: double
}

type Energy: void {
  .percentage: double
}

type Parameters: void {
  .distance?: double
  .stiffness?: double
  .position?: Position
  .period?: double
  .class?: string
}

type Spring: void {
  .id: int
  .name: string
  .parameters?: Parameters
}

type Action: void {
  .id: int
  .name: string
  .parameters?: Parameters
}

type Policy: void {
  .id: int
  .name: string
  .uav: UAV
  .springs*: Spring
  .action: Action
}

type RegisterRequest: UAV

type PositionRequest: void {
  .uav: UAV
  .position: Position
}

type EnergyRequest: void {
  .uav: UAV
  .energy: Energy
}

type AlertRequest:  void {
  .uav: UAV
  .position: Position
  .result: void {
    .accuracy: double
    .class: string
  }
}

type ImageRequest: void {
  .uav: UAV
  .image: void {
    .base64: string
  }
}

type UpdateRequest: void {
  .value: string
}

type Alarm: void {
  .start: long
  .end?: long
}

type StatsRequest: void {
  .alarm : Alarm
}

interface LogicEngineInterface {
  OneWay: 
    register( RegisterRequest ),
    position( PositionRequest ),
    energy( EnergyRequest ),
    alert( AlertRequest ),
    image( ImageRequest ),
    stats( StatsRequest )
}

type PolicyRequest: Policy

interface UAVInterface {
  OneWay: 
    policy( PolicyRequest )
}

type IndexRequest: void

type IndexResponse: string

interface WebServerInterface {
  OneWay: 
    update( UpdateRequest )
  RequestResponse: 
    index( IndexRequest )( IndexResponse )
}

type AlarmRequest: void

type AlarmResponse: any

interface AlarmInterface {
  RequestResponse: alarm( AlarmRequest )( AlarmResponse )
}