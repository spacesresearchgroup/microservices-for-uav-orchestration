include "ini_utils.iol"
include "exec.iol"

include "geometry_utils.iol"
include "google_vision.iol"

include "interface.iol"

outputPort UAV {
  Protocol: coap 
  Interfaces: UAVInterface
}

outputPort AlarmHandler {
  Protocol: http
  Interfaces: AlarmInterface
}

inputPort WebClient {
  Location: "auto:ini:/Locations/TCP_ORCHESTRATOR:file:./../../config.ini"
  Protocol: http
  Interfaces: WebServerInterface
}

inputPort LogicEngine {
  Location: "auto:ini:/Locations/UDP_ORCHESTRATOR:file:./../../config.ini"
  Protocol: coap {
    json_encoding = true
  }
  Interfaces: LogicEngineInterface
}

execution{ concurrent }

init
{
  parseIniFile@IniUtils( "./../../config.ini" )( config ) ;
  global.level = 2 
}

main
{
  
  [ register( registerRequest ) ] {

    global.firstTime = true ;
    for ( i=0, i<#global.uavs, i++ ) {
      if( global.uavs[i].id == registerRequest.id ) {
        global.firstTime = false 
      }
    } ;

    if( global.firstTime ) {

      global.uavs[ #global.uavs ] <<  registerRequest ;
      UAV.location = registerRequest.address ;
             
      with( policyRequest ) {
        .id = int( config.Policy.REGISTER_ID ) ;
        .name = string( config.Policy.REGISTER_NAME ) ;
        .uav.id = registerRequest.id ;
        with( .springs[0] ){
          .id = int( config.Spring.COVER_ID ) ;
          .name = string( config.Spring.COVER_NAME ) ;
          with( .parameters ){
            .distance = double( config.Distance.NORMAL ) ;
            .stiffness = double( config.Stiffness.SOFT ) ;
            .position.x = -1.0 ;
            .position.y = -1.0
          }
        } ;

        if( global.level == 1 || global.level == 2 ) {

          with( .action ){
            .id = int( config.Action.IMAGE_ID ) ;
            .name = string( config.Action.IMAGE_NAME ) ;
            .parameters.period = double( config.Image.RARELY )
          } 
          
        } else if ( global.level == 3 || global.level == 4 ) {
         
          with( .action ){
            .id = int( config.Action.DETECT_ID ) ;
            .name = string( config.Action.DETECT_NAME ) ;
            .parameters.period = double( config.Image.RARELY ) ;
            .parameters.class = string( config.Action.DETECT_CLASS )
          } 

        }
        
      } ;

      policy@UAV( policyRequest ) ; undef(UAV.location) 

    }

  }

  [ position( positionRequest ) ] {

    for ( i=0, i<#global.uavs, i++ ) {
      if( global.uavs[i].id == positionRequest.uav.id ) {
        global.uavs[i].currentPosition << positionRequest.position
      }
    }

  }

  [ energy( energyRequest ) ] {

    for ( i=0, i<#global.uavs, i++ ) {
      if( global.uavs[i].id == energyRequest.uav.id ) {
        global.uavs[i].currentEnergy << energyRequest.energy
      }
    }

  }
    
  [ alert( alertRequest ) ] {

    global.follow_with_policy = true ;
    
    AlarmHandler.location = config.Location.TCP_ALARM ;
    
    // control the accuracy if the label corresponds to the detected class
    if( annotateImageResponse.accuracy > 0.8 ) {
      alarm@AlarmHandler( )( alarmResponse ) ;
      global.follow_with_policy = false ;

      if( alarmResponse ) {
        exec@Exec( "date +%s" )( responseTime ) ;
        global.stats.alarm.end = long( responseTime )
      }
    } ;

    if( global.follow_with_policy ) {
      
      for ( i=0, i<#global.uavs, i++ ) {

        if( global.uavs[i].id == alertRequest.uav.id ) {

          if( global.level == 1 || global.level == 3 ) {

            UAV.location = global.uavs[i].address ; 
            with( policyRequest ) { 
              .id = int( config.Policy.ALERT_STOP_ID ) ;
              .name = string( config.Policy.ALERT_STOP_NAME ) ;
              .uav.id = global.uavs[i].id ;
              with( .springs[0] ){
                .id = int( config.Spring.COVER_ID ) ;
                .name = string( config.Spring.COVER_NAME ) ;
                with( .parameters ){
                  .distance = double( config.Distance.NORMAL ) ;
                  .stiffness = double( config.Stiffness.SOFT ) ;
                  .position.x = -1.0 ;
                  .position.y = -1.0
                }
              } ;
              with( .springs[1] ){
                .id = int( config.Spring.STOP_ID ) ;
                .name = string( config.Spring.STOP_NAME ) ;
                with( .parameters ){
                  .distance = double( config.Distance.STOP ) ;
                  .stiffness = double( config.Stiffness.HARD ) ;
                  .position << alertRequest.position 
                }
              } ;
              with( .action ){
                .id = int( config.Action.NONE_ID ) ;
                .name = string( config.Action.NONE_NAME )
              } 
            } 
            
          } else if ( global.level == 2 || global.level == 4 ) {
           
            UAV.location = global.uavs[i].address ; 
            with( policyRequest ) { 
              .id = int( config.Policy.ALERT_STOP_ID ) ;
              .name = string( config.Policy.ALERT_STOP_NAME ) ;
              .uav.id = global.uavs[i].id ;
              with( .springs ){
                .id = int( config.Spring.COVER_ID ) ;
                .name = string( config.Spring.COVER_NAME ) ;
                with( .parameters ){
                  .distance = double( config.Distance.NORMAL ) ;
                  .stiffness = double( config.Stiffness.SOFT ) ;
                  .position.x = -1.0 ;
                  .position.y = -1.0
                }
              } ;
              with( .action ){
                .id = int( config.Action.NONE_ID ) ;
                .name = string( config.Action.NONE_NAME )
              } 
            } 

          } ;

          policy@UAV( policyRequest ) ; undef(UAV.location) 

        }

      } ; 

      for ( j=0, j<#global.uavs, j++ ) {
        if( global.uavs[j].id != alertRequest.uav.id ) {

          with( euclideanDistanceRequest ) {
            with( .firstPoint ){
              .coordinates[0] = alertRequest.position.x ; 
              .coordinates[1] = alertRequest.position.y 
            } ;
            with( .secondPoint ){
              .coordinates[0] = global.uavs[j].currentPosition.x ;
              .coordinates[1] = global.uavs[j].currentPosition.y
            }
          } ; 

          euclideanDistance@GeometryUtils( euclideanDistanceRequest )( euclideanDistanceResponse ) ;
            
          if( euclideanDistanceResponse <= ( double( config.Distance.FACTOR ) * double( config.Distance.NORMAL ) ) ) {
            
            if( global.level == 1 || global.level == 3 ) {

              UAV.location = global.uavs[j].address ; 
              with( policyRequest ) { 
                .id = int( config.Policy.ALERT_FOCUS_ID ) ;
                .name = string( config.Policy.ALERT_FOCUS_NAME ) ;
                .uav.id = global.uavs[j].id ;
                with( .springs[0] ){
                  .id = int( config.Spring.COVER_ID ) ;
                  .name = string( config.Spring.COVER_NAME ) ;
                  with( .parameters ){
                    .distance = double( config.Distance.NORMAL ) ;
                    .stiffness = double( config.Stiffness.SOFT ) ;
                    .position.x = -1.0 ;
                    .position.y = -1.0
                  }
                } ;
                with( .springs[1] ){
                  .id = int( config.Spring.FOCUS_ID ) ;
                  .name = string( config.Spring.FOCUS_NAME ) ; 
                  with( .parameters ){
                    .distance = double( config.Distance.CLOSE ) ;
                    .stiffness = double( config.Stiffness.MEDIUM ) ;
                    .position << alertRequest.position
                  }
                } 

              } 
              
            } else if ( global.level == 2 || global.level == 4 ) {
             
              UAV.location = global.uavs[j].address ; 
              with( policyRequest ) { 
                .id = int( config.Policy.ALERT_FOCUS_ID ) ;
                .name = string( config.Policy.ALERT_FOCUS_NAME ) ;
                .uav.id = global.uavs[j].id ;
                with( .springs ){
                  .id = int( config.Spring.COVER_ID ) ;
                  .name = string( config.Spring.COVER_NAME ) ;
                  with( .parameters ){
                    .distance = double( config.Distance.NORMAL ) ;
                    .stiffness = double( config.Stiffness.SOFT ) ;
                    .position.x = -1.0 ;
                    .position.y = -1.0
                  }
                } 
              } 

            } ;

            if( global.level == 1 || global.level == 2 ) {

              with( policyRequest ) {
                with( .action ){
                  .id = int( config.Action.IMAGE_ID ) ;
                  .name = string( config.Action.IMAGE_NAME ) ;
                  .parameters.period = double( config.Image.OFTEN )
                } 
              }

            } else if ( global.level == 3 || global.level == 4 ) {

              with( policyRequest ) {           
                with( .action ){
                  .id = int( config.Action.DETECT_ID ) ;
                  .name = string( config.Action.DETECT_NAME ) ;
                  .parameters.period = double( config.Image.OFTEN )
                } 
              }

            } ;

            policy@UAV( policyRequest ) ; undef(UAV.location) 

          }
        }
      }
    }
  
  }

  [ image( imageRequest ) ] {

    with( annotateImageRequest) {
      with( .requests[0] ) {
        // this must be a static image 
        .image.content = imageRequest.image.base64 ;
        with( .features ) {
          .type = "LABEL_DETECTION" ;
          .model = "builtin/latest" ;
          .maxResults = 10 
        }
      }
    } ;
    annotateImageRequest.fields = "responses(labelAnnotations(description,score))" ;
    annotateImageRequest.key = string( config.Image.API_KEY) ;
    annotateImage@GoogleCloudVision( annotateImageRequest )( annotateImageResponse ) ;

    AlarmHandler.location = config.Location.TCP_ALARM ;
    
    if( annotateImageResponse.accuracy > 0.8 ) {
      alarm@AlarmHandler( )( alarmResponse ) ;

      if( alarmResponse ) {
        exec@Exec( "date +%s" )( responseTime ) ;
        global.stats.alarm.end = long( responseTime )
      }
    }

  }

  [ update( updateRequest ) ] {

    global.level = int( updateRequest.value )

  }

  [ stats( statsRequest ) ] {

    global.stats << statsRequest

  }

}
