type Image: void {
  .content?: string
  .source?: void {
    .gscImageUri?: string
    .imageUri?: string
  }
}

type Feature: void {
  .type: string
  .maxResults?: int
  .model?: string
}

type LatLng: void {
  .latitude: double
  .longitude: double
}

type BoundingPoly: undefined

type ImageContext: void {
  .latLongRect?: void {
    .minLatLng?: LatLng
    .maxLatLng?: LatLng
  }
  .languageHints*: string
  .cropHintsParams?: void {
    .aspectRatios*: double
  }
  .productSearchParams?: void {
    .boundingPoly?: BoundingPoly
    .productSet?: string
    .productCategories*: string
    .filter?: string
  }
  .webDetectionParams?: void {
    .includeGeoResults: bool
  }
}

type AnnotateImageRequest: void {
  .fields: string
  .key: string
  .requests*: void {
    .image: Image
    .features*: Feature
    .imageContext?: ImageContext 
  }
}

type LocationInfo: undefined

type Property: undefined

type EntityAnnotation: void {
  .mid?: string
  .locale?: string
  .description?: string
  .score?: double
  .confidence?: double
  .topicality?: double
  .boundingPoly?: BoundingPoly
  .locations*: LocationInfo 
  .properties*: Property
}

type WebEntity: void {
  .entityId?: string
  .score?: double
  .description?: string
}

type WebImage: void {
  .url?: string
  .score?: double
  .pageTitle?: string
  .partialMatchingImages?: void {
    .url: string
  }
}

type WebLabel: void {
  .label?: string
  .languageCode?: string
}

type WebDetection: void {
  .webEntities?: WebEntity
  .fullMatchingImages?: WebImage
  .partialMatchingImages?: WebImage
  .pagesWithMatchingImages?: WebImage
  .visuallySimilarImages?: WebImage
  .bestGuessLabels?: WebLabel
}

type Status: void {
  .status?: string
  .code?: double
  .message?: string
  .details*: undefined
}

type AnnotateImageResponse: void {
  .responses*: void {
    .labelAnnotations*: EntityAnnotation
    .webDetection?: WebDetection
    .context?: void {
      .uri?: string
      .pageNumber?: double
    }
  }
  .error?: Status
}

type GoogleVisionFaultType: void {
  .exceptionMessage: string
}

interface GoogleCloudVisionInterface {
  RequestResponse: 
    annotateImage( AnnotateImageRequest )( AnnotateImageResponse ) throws 
      GoogleVisionBadRequest( GoogleVisionFaultType )
}

outputPort GoogleCloudVisionAPI {
  Location: "socket://vision.googleapis.com:443"
  Protocol: https {
    .format = "json" ;
    .osc.annotateImage.alias = "/v1/images:annotate?fields=%!{fields}&key=%!{key}"
  }
  Interfaces: GoogleCloudVisionInterface
}

service GoogleCloudVision 
{
  Interfaces: GoogleCloudVisionInterface
  main 
  {
    annotateImage( annotateImageRequest )( annotateImageResponse ) {

      install( 
        TypeMismatch => 
          faultError.exceptionMessage = "You missed to specify the \"fields\" or the API \"key\" for Google Cloud Vision in the request!" ;
          throw( GoogleVisionBadRequest, faultError ),
        GoogleVisionBadRequest => 
          nullProcess
      ) ;

      annotateImage@GoogleCloudVisionAPI( annotateImageRequest )( annotateResponse ) ;
      if( is_defined( annotateResponse.error ) ) {
        faultError.exceptionMessage = annotateResponse.error.message ;
        throw( GoogleVisionBadRequest, faultError )
      } else {
        annotateImageResponse << annotateResponse
      }
      
    }
  }
}