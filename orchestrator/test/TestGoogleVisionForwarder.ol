include "file.iol"
include "console.iol"
include "string_utils.iol"

include "../src/google_vision.iol"

main
{
  getServiceDirectory@File()( dir ) ;
    with( annotateImageRequest) {
      .fields = "responses(labelAnnotations(description,score))" ;
      .key = API_KEY ;
      with( .requests[0] ) {
        .image.source.imageUri = "http://rudydegger.com/wp-content/uploads/2018/01/Picture15.jpg" ;
        with( .features ) {
          .type = "LABEL_DETECTION" ;
          .maxResults = 1 
        }
      }
    } ;
    annotateImage@GoogleCloudVision( annotateImageRequest )( annotateImageResponse ) ;
    valueToPrettyString@StringUtils( annotateImageResponse )( pp ) ;
    println@Console( pp )()
}
