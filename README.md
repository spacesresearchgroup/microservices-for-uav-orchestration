# A case study on Microservice Oriented Architectures in Development of a Traffic Collision Detection System deployed by UAVs

- [Maurizio Gabbrielli](http://cs.unibo.it/~gabbri)\*
- [Marco Di Felice](http://cs.unibo.it/~difelice)\*
- [Ivan Lanese](http://cs.unibo.it/~ilanese)
- [Angelo Trotta](http://angelotrotta.com/en/)\*
- [Stefano Pio Zingaro](http://cs.unibo.it/~stefanopio.zingaro)\*

\*Department of Computer Science and Engineering, School of Science Università di Bologna, Italy

*In this work we propose a centralized, microservice oriented approach to handle emergency scenarios dynamically. We validate our approach orchestrating UAVs in a traffic collision detection system.*

# Introduction

Introducing the problem... all together...

# Related Work

Here we go with related... just one of us...

# Architecture Overview

Consider a simple case where, for given task and a set of dynamic sensors, we implemented a specific strategy that fulfills the task and, at a certain moment of the computation on the on-line system, we need to change that strategy. Note that we are considering not just a different configuration of the parameters, used by the strategy, but more complex patterns, such as changing the specific action required by the task.







...
In principle, the container that act as forwarder service to requests for image analysis is intended as a cloud service.
The service deployed in this container could perform any detection task, supposed that the interface remains the same (name of the operation + request and response type). This gives the system the possibility to change objective, even at runtime. 
The second container deploys the orchestrator service that interacts with the drones (UAVs in this scenario), acting as the logic engine.
Each one of the containers come with its own test suite. 
...

## Contribution

The main contribution of this work is to build a system that has a dynamic behavior. This behavior can be modified at runtime by switching between four possible optimizations levels. These levels go from from maximizing the accuracy of the threat detection and minimizing the alarm delay completion time.

## Software Technologies Involved

- The [Jolie](http://jolie-lang.org) programming language is provided as a *JAVA* interpreter running on top of the *JAVA Virtual Machine* (JDK >= 1.6 required). The interpreter sources are available on [Github](https://github.com/jolie/jolie). The installer [JAR](https://www.jolie-lang.org/files/releases/jolie-1.7.0.jar) executable and instruction for installation can be found [here](https://www.jolie-lang.org/downloads.html). A *Docker* solution for a self-contained development environment is also available via `docker pull jolielang/jolie` command.
- The [JIoT](http://www.cs.unibo.it/projects/jolie/jiot.html) project consists of a fork of the Jolie Programming Language that supports IoT-related protocols ([CoAP](https://tools.ietf.org/html/rfc7252) and [MQTT](http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html)). Sources are available on [Github](https://github.com/saltgz/jolie/tree/jiot). Please note that the version used in the project refers to the [JIoT 1.1](http://www.cs.unibo.it/projects/jolie/jiot/jiot-1.1.jar) jar installer in the [website of the project](http://www.cs.unibo.it/projects/jolie/jiot.html). A *Docker* solution, as in the case of Jolie, is available through `docker pull saltgz/jiot` command.

# Case Study Description

The case study of the project consists in the development of a traffic collision detection system, able to generate real threat alerts with high confidence. 
The alert itself is then 

In our implementation alarms are handled by a REST service, acting as a an emergency alerts dispatcher.

The detection system is deployed by *unmanned vehicles* (UVs), in this specific case study by *aerial* ones (UAVs). 
The actions that a single UAV could perform are: 
- covering the area, 
- taking pictures with its equipped camera,
- and detect collisions by analyzing the images produced. 

With the term **policy** we refer to the set of actions, the set of springs, and the parameters.

The strategy ...

The implemented architecture follows the principle of microservice oriented computing. This means that every functionalities in the system is provided as a microservice. In order to coordinate (or better to orchestrate) the interaction between the services we need an orchestrator, that is a microservice as well. 
To accomplish a smooth development of the orchestration platform and, in general, of the microservices, we used the [Jolie](http://jolie-lang.org) programming language, a concurrency and microservice oriented programming language that guarantees interoperability of heterogeneous platforms in a transparent way for the developer (both from the data representation and application protocols points of view). 

In the use case we also take advantage of `Docker` containers to provide the microservices as standalone executable and to separate the different development environments used in the project. The description of the containers and a list of commands to run a demo of the system is provided below.

## Minor Contribution

- The Jolie **interface** `GeometryUtils` in *orchestrator/src/geometry_utils.iol*, is a small utility to deal with *Euclidean Distance* computation between two points.
The typical usage would be:
```jolie
include 'geometry_utils.iol'

main 
{
  // define euclideanDistanceRequest
  euclideanDistance@GeometryUtils( euclideanDistanceRequest )( euclideanDistanceResponse ) ;
  // do whatever with euclideanDistanceResponse
}
```
where the *request* type `EuclideanDistanceRequest` is defined as a tree structure with empty root, containing multiple (\*) `double` coordinates for two points.
```jolie
type EuclideanDistanceRequest: void {
  .firstPoint: void {
    .coordinates*: double
  }
  .secondPoint: void {
    .coordinates*: double
  }
}
```
and the *response* type `EuclideanDistanceResponse` is defined as a simple double representing the distance.
```jolie
type EuclideanDistanceResponse: double
```

- In `google_vision.iol` ...

# Performance Evaluation

## Local Deployment of the Architecture

```
/                  ------------                   --------------                    -------------         
\                 | Web Server |                 | Orchestrator |---| 5684/UDP |-->| UVs Network | 
/--| 8080/TCP |-->| localhost  |--| 9000/TCP |-->|  localhost   |<--| 5683/UDP |---|  localhost  |
\                  ------------                   --------------                    -------------
/                                                     |                  ------------
\                                                     |                 |   Alarm    |
/                                                      --| 9001/TCP |-->| localhost  | 
\                                                                        ------------
```

### Demo Instructions

A set of instruction to properly run a local demo of the system.

1. Run **alarm handler** service in *alarm_handler/src/AlarmHandler.ol* that will receive the alarm final communication.
```
jolie AlarmHandler.ol
```

2. Run the **orchestrator** service in *orchestrator/src/LogicEngine.ol* sending policies to the the UVs.
```
jolie LogicEngine.ol
```
Note that, if you are running MAC OSx with default `sysctl` configurations, it is needed to increase the **datagram maximum packet size** of the machine. This issue can be solved with the command:
```
sudo sysctl net.inet.udp.maxdgram=64000
```

3. Open the **web server** in *web_server/index.html* waiting for user input to choose the UVs strategy, in your preferred browser (supporting Javascript).

4. Run the **uav** simulator service in *uav_network/src/Uav1.ol*.
```
jolie Uav1.ol
```

## Container Deployment of the Architecture

The subnet used in the containerized version of the architecture, that has to be specified in the `run` Docker command, is a **user-defined network**. 
Docker allows us to assign static IPs to the containers with the following command:

``` 
docker network create --subnet=172.19.0.0/16 juv_net
```

In order to retrieve the IP for the created container, and thus to check the consistency with the user-defined network IP above, we can use the following command:

```
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' <container_id>
```

In Figure 2 we show the infrastructure of the network used in the containerized version of the case study.

```
/                  ------------                   --------------                    -------------         
\                 | Web Server |                 | Orchestrator |---| 5684/UDP |-->| UVs Network | 
/--| 8080/TCP |-->| 172.19.0.4 |--| 9000/TCP |-->|  172.19.0.3  |<--| 5683/UDP |---|  172.19.0.6 |
\                  ------------                   --------------                    -------------
/                                                     |                  ------------
\                                                     |                 |   Alarm    |
/                                                      --| 9001/TCP |-->| 172.19.0.5 | 
\                                                                        ------------
```

### Demo Instruction

A set of instructions to properly run a working demo of the system using Docker containers.

1. Run the Jolie (v. 1.7.0) container acting as the **alarm handler** service that will receive the alarm final communication.
```
docker run --name alarm_handler -it --rm -v "$(pwd)"/alarm_handler:/home/ --net juv_net --ip 172.19.0.5 -p 9001:9001 -w /home/src/ jolielang/jolie jolie AlarmHandler.ol
```

2. Run the JIoT (v. 1.0) container acting as the UVs **orchestrator**.
```
docker run --name orchestrator -it --rm -v "$(pwd)"/orchestrator:/home/ --net juv_net --ip 172.19.0.3 -p 9000:9000 -p 5683:5683/udp -w /home/src/ saltgz/jiot jolie LogicEngine.ol
```

3. Run the Jolie Leonardo container acting as a **web server** waiting for user input to choose the UVs strategy.
```
docker run --name web_server -it --rm -v "$(pwd)"/web_server:/web -e LEONARDO_WWW=/web --net juv_net --ip 172.19.0.4 -p 8080:8080 jolielang/leonardo
```

4. Run the JIoT (v. 1.0) container acting as an **uav network** that simulates a uav (run multiple times to run up to three UAVs).
```
docker run --name uav_network -it --rm -v "$(pwd)"/uav_network:/home/ --net juv_net --ip 172.19.0.6 -p 5684:5684/udp -w /home/src/ saltgz/jiot jolie Uav1.ol
```
```
docker run --name uav_network -it --rm -v "$(pwd)"/uav_network:/home/ --net juv_net --ip 172.19.0.7 -p 5685:5685/udp -w /home/src/ saltgz/jiot jolie Uav2.ol
```
```
docker run --name uav_network -it --rm -v "$(pwd)"/uav_network:/home/ --net juv_net --ip 172.19.0.8 -p 5686:5686/udp -w /home/src/ saltgz/jiot jolie Uav3.ol
```

# Conclusion and Future Work

Here the conclusion and the possible extensions of the work... I think Marco is the best suited for this
