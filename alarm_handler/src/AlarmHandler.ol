include "file.iol"
include "ini_utils.iol"

type AlarmRequest: void
type AlarmResponse: any

interface AlarmInterface {
  RequestResponse: alarm( AlarmRequest )( AlarmResponse )
}

inputPort AlarmHandler {
  Location: "auto:ini:/Locations/TCP_ALARM:file:../../config.ini"
  Protocol: http
  Interfaces: AlarmInterface
}

execution{ sequential }

main
{
  alarm( alarmRequest )( true )
}